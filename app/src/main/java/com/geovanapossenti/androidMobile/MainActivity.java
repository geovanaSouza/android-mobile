package com.geovanapossenti.androidmobile;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<String> alunos = new ArrayList<>(
                Arrays.asList("Adriano", "Clayton", "Emilio",
                        "Fernando", "Geovana", "Gustavo", "Jeferson", "João Victor", "Jonathan", "Juliano", "Tiago", "Victor"));
        TextView primeiroAluno = findViewById(R.id.textView);
        TextView segundoAluno = findViewById(R.id.textView2);
        TextView terceiroAluno = findViewById(R.id.textView3);
        TextView quartoAluno = findViewById(R.id.textView4);
        TextView quintoAluno = findViewById(R.id.textView5);
        TextView sextoAluno = findViewById(R.id.textView6);
        TextView setimoAluno = findViewById(R.id.textView7);
        TextView oitavoAluno = findViewById(R.id.textView8);
        TextView nonoAluno = findViewById(R.id.textView9);
        TextView decimoAluno = findViewById(R.id.textView10);
        TextView decimoPrimeiroAluno = findViewById(R.id.textView11);
        TextView decimoSegundoAluno = findViewById(R.id.textView12);
        primeiroAluno.setText(alunos.get(0));
        segundoAluno.setText(alunos.get(1));
        terceiroAluno.setText(alunos.get(2));
        quartoAluno.setText(alunos.get(3));
        quintoAluno.setText(alunos.get(4));
        sextoAluno.setText(alunos.get(5));
        setimoAluno.setText(alunos.get(6));
        oitavoAluno.setText(alunos.get(7));
        nonoAluno.setText(alunos.get(8));
        decimoAluno.setText(alunos.get(9));
        decimoPrimeiroAluno.setText(alunos.get(10));
        decimoSegundoAluno.setText(alunos.get(11));

    }
}
